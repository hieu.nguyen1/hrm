import * as log from 'loglevel';


import "/imports/hrm-app-api/api/index";
declare var BrowserPolicy;

log.setLevel(0, true)

Meteor.startup(function() {
  console.log('Configuring content-security-policy');
  BrowserPolicy.content.allowSameOriginForAll();
  BrowserPolicy.content.allowOriginForAll('http://fonts.googleapis.com');
  BrowserPolicy.content.allowOriginForAll('http://fonts.gstatic.com');
  BrowserPolicy.content.allowOriginForAll('https://maxcdn.bootstrapcdn.com');
  BrowserPolicy.content.allowEval();
  BrowserPolicy.framing.disallow();
});