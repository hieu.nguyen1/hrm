import 'reflect-metadata';
import 'zone.js/dist/zone';
import { Meteor } from 'meteor/meteor';
import * as log from 'loglevel';

let run:any;
declare let require:any;

import { PlatformTools, TargetPlatformId } from '../imports/common-app/src/ui-ng2/platform-tools/platform-tools';

  console.log("Setting platform to TWBS_WEB");
  PlatformTools.setTargetPlatform(TargetPlatformId.TWBS_WEB);

  run =  require("../imports/hrm-app-web/app.module").run;


import { enableProdMode } from '@angular/core';


console.log("In apps.ts @" + new Date());
console.log(Meteor.absoluteUrl());

if (Meteor.isProduction) {
  console.log("Production environment detected.  Log level set to error");
  log.setLevel(LogLevel.ERROR);
  enableProdMode();
} else {
  console.log("Development environment detected.  Log level set to info");
  log.setLevel(LogLevel.TRACE);
}


  run();

