import { Meteor } from 'meteor/meteor';
import  'meteor/aldeed:simple-schema';
import * as log from 'loglevel';

export let CompanyCollection = new Mongo.Collection("Company");

export class Company{
    _id: string;
    name: string;
    alias: string;
}
export interface CompanyInterface extends Company{}

let CompanySchema = new SimpleSchema({
    name: {
        type: String,
        unique: true,
    },
    alias: {
        type: String,
        optional: true
    }
});

// CompanyCollection.allow({});
CompanyCollection.attachSchema(CompanySchema);

if(Meteor.isServer){
    // Meteor.publish();
    Meteor.methods({
        checkCompany: function (companyName:string): CompanyInterface{
            let company: Company = <Company>CompanyCollection.findOne({name:companyName});
            if(!company){
                throw new Meteor.Error('Not-Found-Company','Not Found company '+ companyName);
            } else {
                return company;
            }
        },
        checkCompanyExistByString: function (companyName:string):boolean{
            let count:number = <number>CompanyCollection.find({name:companyName}).count();
            if(count > 0){
                throw new Meteor.Error("name-company-existed", "Name company " + companyName + " alrealy Existed");
            } else {
                return true;
            }
        },
        registerCompany: function(companyName:string) {
            return CompanyCollection.insert({name: companyName});
        },
        editCompany: function (company: Company){
            return CompanyCollection.update(company._id,{
                $set: company
            });
        }
    });
    Meteor.startup(function () {
        if (CompanyCollection.find({}).count() === 0 ){
            log.info("seed company database");
            CompanyCollection.insert({ "_id" : "GsDRtiqG9efm9DM4R", "name" : "HDWebsoft", "alias" : "HDWebsoft" });
        }
    })
}
