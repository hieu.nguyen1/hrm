/**
 * Copyright Ken Ono, Fabrica Technolology 2016
 * Source code license under Creative Commons - Attribution-NonCommercial 2.0 Canada (CC BY-NC 2.0 CA)
 */
import { Component, NgZone } from '@angular/core';
import { select } from 'ng2-redux';

import {ILoginState} from "../../common-app/src/ui/redux/login/login.types";
import {CompanyActions} from "../redux";
// import {IConnectState} from "../../common-app/src/ui/redux/connect/connect.types";


@Component({
    templateUrl: './company.template.html',
    selector: 'company'
})
export class CompanyComponent {
    @select() loginReducer;
    // @select() connectReducer;
    state:ILoginState;
    // connectionState:IConnectState;
    nameCompany: string;
    active:boolean = true;
    constructor(private ngZone:NgZone) {}
    ngOnInit() {
        this.loginReducer.subscribe( (state:ILoginState)=>{  /// Hmm.  Is there a way of doing this automatically?
            this.state = state;
        });
        // this.connectReducer.subscribe( (state:IConnectState)=>{
        //         this.ngZone.run( ()=>{
        //             this.connectionState = state;
        //         } );
        //     }
        // );
    }
    ngAfterContentInit() {
        this.reset();
    }

    private reset() {
        this.active = false;  // Forces reset as per https://angular.io/docs/ts/latest/guide/forms.html
                              // This is a temporary workaround while we await a proper form reset feature.
        setTimeout(()=> this.active=true, 0);
    }
    registerWithNameCompany(){
        CompanyActions.newCompany(this.nameCompany);
    }
    access(){
        CompanyActions.accessCompany(this.nameCompany);
    }


}