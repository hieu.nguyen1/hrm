import {Component, OnInit, NgZone} from '@angular/core';
import {Router} from '@angular/router'

import {Menus} from "../common-app/src/ui/services/menus";
import {MenuItem} from "../common-app/src/ui/services/menu-item";
import {LoginActions} from "../common-app/src/ui/redux/login/login-actions.class";
import { User } from "../common-app-api/src/api/models/user.model";
import {ILoginState, LOGIN_INITIAL_STATE} from "../common-app/src/ui";

import {CompanyReduxModule} from './redux';


import {ReduxModules} from "./top-frame/redux-modules";
import {TopFrame} from "./top-frame/top-frame.base";
import {select} from "ng2-redux";

@Component(
    {
        selector: 'hrm-top-frame',
        templateUrl: './app.template.html'
    }
)
export class HRMTopFrame extends TopFrame implements OnInit {
    public user:User;
    @select() loginReducer;
    constructor(private router: Router,
                private ngZone: NgZone,
                private reduxModules:ReduxModules,
                companyReduxModule:CompanyReduxModule) {
        super();
        this.loginReducer.subscribe( (loginState:ILoginState)=>{
            loginState = loginState || LOGIN_INITIAL_STATE;
            this.user = loginState.user;
        });
        this.addMiddlware(companyReduxModule);
        reduxModules.configure();
        Menus.addMenu({id: 'topbar'});


        Menus.addSubMenuItem('topbar', {
            id: 'edit-company',
            title: 'Edit Your Company',
            roles: ['Company Owner'],
            callback: ()=> {
                this.navigateToEditCompany();
            }
        });


        Menus.addSubMenuItem('topbar', {
            id: 'edit-user-profile',
            title: 'Profile',
            callback: ()=> {
                this.navigateToProfile();
            }
        });


        Menus.addSubMenuItem('topbar', {
            id: 'logout',
            title: 'Logout',
            roles: ['*'],
            callback: (menuItem: MenuItem)=> {
                LoginActions.logout();
            }
        });

    }

    ngOnInit() {
    }

    navigateToEditCompany() {
        this.ngZone.run(()=> {
            this.router.navigate(['/edit-company']);
        });
    }

    navigateToProfile() {
        this.ngZone.run(()=> {
            this.router.navigate(['/edit-profile']);
        });
    }

    navigateToLoginUser() {
        this.ngZone.run(()=> {
            this.router.navigate(['/login-user']);
        });
    }

    navigateToRegisterCompany(){
        this.ngZone.run(()=> {
            this.router.navigate(['/register-company']);
        });
    }
    navigateToWelcome(): void {
        this.ngZone.run(()=> {
            this.router.navigate(['/']);
        });
    }

}