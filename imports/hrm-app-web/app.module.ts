import { ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {DragulaModule} from 'ng2-dragula/ng2-dragula';
import {TabsModule} from 'ng2-bootstrap/ng2-bootstrap';

import {CommonAppNgTWBS} from "../common-app/src/ui-twbs-ng2/common-app-ng-twbs.module";
import {COMMON_APP_SINGLETONS} from "../common-app/src/ui-ng2/common-app-ng.module";
import {HRMTopFrame} from './app.component';

import {LoginUserComponent} from "./Login-User/login.user";

// import {TopHeaderModule} from "./header/top-frame-header.module";
import {ReduxModules} from "./top-frame/redux-modules";
import {CompanyComponent} from "./company/company.component";
import {CompanyReduxModule} from "./redux/company/company-redux.module";
import {CompanyAsync} from "./redux/company/company-redux.async";
import {RegisterCompanyComponent} from "./register-company/register-company.component";
import {EditCompanyComponent} from "./edit-company/edit-company.component";
import {EditUserProfileTWBS} from "./edit-user-profile/edit-user-profile.twbs";
import {FormsModule} from "@angular/forms";
import {TopFrameHeader} from "./header/top-frame-header";

const appRoutes: Routes = [
    {path: '', component: CompanyComponent},
    {path: 'login-user', component: LoginUserComponent},
    {path: 'register-company', component: RegisterCompanyComponent},
    {path: 'edit-company', component: EditCompanyComponent},
    {path: 'edit-profile', component: EditUserProfileTWBS}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
@NgModule({
    imports: [BrowserModule, CommonAppNgTWBS, DragulaModule, routing, TabsModule, FormsModule],
    declarations: [
        HRMTopFrame,
        TopFrameHeader,
        LoginUserComponent,
        RegisterCompanyComponent,
        EditCompanyComponent,
        EditUserProfileTWBS,
        CompanyComponent
    ],
    bootstrap: [HRMTopFrame],
    providers: [
        ReduxModules,
        CompanyReduxModule,
        CompanyAsync,
        ...COMMON_APP_SINGLETONS]
})
export class AppModule {
}


export function run() {
    const platform = platformBrowserDynamic();
    platform.bootstrapModule(AppModule);
}

export function prepare(): void {

}