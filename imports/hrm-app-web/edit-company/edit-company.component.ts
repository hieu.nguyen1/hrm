import { Component, NgZone } from '@angular/core';
import { select } from 'ng2-redux';
import * as log from 'loglevel';


import {ILoginState} from "../../common-app/src/ui/redux/login/login.types";
import {ICompanyState} from "../redux/company/company-redux.types";
import {Company} from "../../hrm-app-api/api/models/company.model";
import {Router} from "@angular/router";
import {User} from "../../common-app-api/src/api/models/user.model";
import {CompanyActions} from "../redux/company/company-redux.actions";


@Component({
    templateUrl: 'edit-company.template.html',
    selector: 'edit-company'
})
export class EditCompanyComponent {
    @select() CompanyReducer;
    @select() loginReducer;
    user: User;
    company:Company;
    state:ILoginState;
    active:boolean = true;
    constructor(private ngZone:NgZone, private router: Router) {}
    ngOnInit() {
        this.CompanyReducer.subscribe((state:ICompanyState) => {
            if(state.company){
                this.company = state.company;
            } else {
                this.ngZone.run(()=> {
                    this.router.navigate(['/']);
                });
            }
        });
        this.loginReducer.subscribe( (state:ILoginState)=>{
            this.state = state;
            if(state.user){
                this.user = state.user;
            } else{
                this.ngZone.run(()=> {
                    this.router.navigate(['/login-user']);
                });
            }
        });
        if(this.user.profile.nameCompany !== this.company.name){
            this.ngZone.run(()=> {
                this.router.navigate(['/login-user']);
            });
        }

    }
    ngAfterContentInit() {
        this.reset();
    }

    private reset() {
        this.active = false;  // Forces reset as per https://angular.io/docs/ts/latest/guide/forms.html
                              // This is a temporary workaround while we await a proper form reset feature.
        setTimeout(()=> this.active=true, 0);
    }

    editCompany() {
        CompanyActions.editCompany(this.company);
    }

}