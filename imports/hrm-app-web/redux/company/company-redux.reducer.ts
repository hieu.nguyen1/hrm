
import { IPayloadAction } from '../../../common-app/src/ui/redux/action.interface';
import {CompanyActions} from './company-redux.actions'
import {ICompanyState, ICompanyActionPayload} from "./company-redux.types";

export const LOGIN_COMPANY_INITIAL_STATE:ICompanyState= {
    editing: false,
    edited: false,
    registered: false,
    registering: false,
    neverLoggedInCompany: true,
    loggedInCompany: false,
    loggingInCompany: false,
    company: null,
    nameCompany: null,
    errorMessage: ''
};

export function CompanyReducer(
    state: ICompanyState = LOGIN_COMPANY_INITIAL_STATE,
    action: IPayloadAction): ICompanyState {
    let payload:ICompanyActionPayload = action.payload;
    switch (action.type) {
        case CompanyActions.ACCESS:
            return Object.assign({}, state, {
                loggingInCompany: true
            });
        case CompanyActions.ACCESS_SUCCESS:
            return Object.assign({}, state,{
                neverLoggedInCompany:false,
                loggingInCompany: false,
                loggedInCompany: true,
                company: payload.company,
                nameCompany: payload.company.name,
                errorMessage: ''
                }
            );
        case CompanyActions.ACCESS_FAIL:
            return Object.assign({}, state,
                {
                    neverLoggedInCompany: false,
                    loggingInCompany: false,
                    loggedInCompany: false,
                    company: null,
                    nameCompany: null,
                    errorMessage: action.error.message
                }
            );
        case CompanyActions.NEW_NAME:
            return Object.assign({}, state, {loggingInCompany: true});
        case CompanyActions.NEW_NAME_SUCCESS:
            return Object.assign({}, state, {
                neverLoggedInCompany:false,
                loggingInCompany: false,
                loggedInCompany: true,
                company: null,
                nameCompany: payload.nameCompany,
                errorMessage: ''
            });
        case CompanyActions.NEW_NAME_FAIL:
            return Object.assign({}, state, {
                neverLoggedInCompany: false,
                loggingInCompany: false,
                loggedInCompany: false,
                company: null,
                nameCompany: null,
                errorMessage: action.error.message
            });
        case CompanyActions.REGISTER:
            return Object.assign({}, state, {
                registering: true,
            });
        case CompanyActions.REGISTER_SUCCESS:
            return Object.assign({}, state, {
                registering: false,
                registered: true,
                company: payload.company,
                nameCompany: payload.company.name,
            });
        case CompanyActions.REGISTER_FAIL:
            return Object.assign({}, state, {
                registering: false,
                registered: false,
                errorMessage: action.error.message
            });
        case CompanyActions.EDIT:
            return Object.assign({}, state, {
                editing: true,
            });
        case CompanyActions.EDIT_SUCCESS:
            return Object.assign({}, state, {
                editing: false,
                edited: true,
                company: payload.company,
                nameCompany: payload.company.name,
            });
        case CompanyActions.EDIT_FAIL:
            return Object.assign({}, state, {
                editing: false,
                edited: false,
                errorMessage: action.error.message
            });
        default:
            return state;
    }
}

