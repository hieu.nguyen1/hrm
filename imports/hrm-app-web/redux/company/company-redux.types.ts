
import {IAppState} from "../../../common-app/src/ui/redux/state.interface";
import {Company} from "../../../hrm-app-api/api/models/company.model";

export interface ICompanyState extends IAppState {
    editing: boolean,
    edited: boolean,
    registered: boolean,
    registering: boolean,
    neverLoggedInCompany: boolean,
    loggedInCompany: boolean,
    loggingInCompany: boolean,
    company: Company,
    nameCompany: string,
    errorMessage: string
}

export interface ICompanyActionPayload {
    nameCompany?:string,
    company?:Company
}
