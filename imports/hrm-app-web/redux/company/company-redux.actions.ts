import {ReduxModuleUtil} from "../../../common-app/src/ui/redux/redux-module-util";
import {ReduxModuleCombiner} from "../../../common-app/src/ui/redux/redux-module-combiner";
import {Credentials} from '../../../common-app/src/ui/services/credentials';
import {Company} from "../../../hrm-app-api/api/models/company.model";
export class CompanyActions {
    private static prefix = 'COMPANY';
    static REGISTER = CompanyActions.prefix + 'REGISTER';
    static REGISTER_FAIL = CompanyActions.prefix + 'REGISTER_FAIL';
    static REGISTER_SUCCESS = CompanyActions.prefix + 'REGISTER_SUCCESS ';
    static ACCESS = CompanyActions.prefix + 'ACCESS';
    static ACCESS_FAIL = CompanyActions.prefix + 'ACCESS_FAIL';
    static ACCESS_SUCCESS  = CompanyActions.prefix + 'ACCESS_SUCCESS';
    static NEW_NAME = CompanyActions.prefix + 'NEW_NAME';
    static NEW_NAME_FAIL = CompanyActions.prefix + 'NEW_NAME_FAIL';
    static NEW_NAME_SUCCESS = CompanyActions.prefix + 'NEW_NAME_SUCCESS';
    static EDIT = CompanyActions.prefix + 'EDIT';
    static EDIT_FAIL = CompanyActions.prefix + 'EDIT_FAIL';
    static EDIT_SUCCESS = CompanyActions.prefix + 'EDIT_SUCCESS';


    static newCompany(name:string) : void {
        ReduxModuleCombiner.ngRedux.dispatch({type:CompanyActions.NEW_NAME, payload: {nameCompany:name}});
    }
    static accessCompany(name:string) : void {
        ReduxModuleCombiner.ngRedux.dispatch({type:CompanyActions.ACCESS, payload: {nameCompany:name}});
    }

    static accessCompanySuccess(company: Company): void {
        ReduxModuleCombiner.ngRedux.dispatch({type:CompanyActions.ACCESS_SUCCESS, payload: {company: company}})
    }
    static newNameSuccess(nameCompany: string):void {
        ReduxModuleCombiner.ngRedux.dispatch({type:CompanyActions.NEW_NAME_SUCCESS, payload: {nameCompany: nameCompany}})
    }
    static registerCompanySuccess(company: Company):void {
        ReduxModuleCombiner.ngRedux.dispatch({type:CompanyActions.NEW_NAME_SUCCESS, payload: {company: company}})
    }
    static registerCompany(nameCompany:string){
        ReduxModuleCombiner.ngRedux.dispatch({type:CompanyActions.REGISTER, payload: {nameCompany:nameCompany}})
    }
    static editCompany(company: Company){
        ReduxModuleCombiner.ngRedux.dispatch({type:CompanyActions.EDIT, payload: {company: company}});
    }
    static editCompanySuccess(company: Company){
        ReduxModuleCombiner.ngRedux.dispatch({type:CompanyActions.EDIT_SUCCESS, payload: {company: company}});
    }

    static error(errorType, error) {
        ReduxModuleCombiner.ngRedux.dispatch( ReduxModuleUtil.errorFactory(errorType, error) );
    }
}
