import {Injectable} from "@angular/core";

import {CompanyReducer} from "./company-redux.reducer";
import {CompanyActions} from "./company-redux.actions";
import {CompanyAsync} from "./company-redux.async";
import {ReduxModule} from "../../../common-app/src/ui/redux/redux-module.class";
import {IAppState} from "../../../common-app/src/ui/redux/state.interface";
import {IPayloadAction} from "../../../common-app/src/ui/redux/action.interface";

@Injectable()
export class CompanyReduxModule extends ReduxModule<IAppState, IPayloadAction>  {
    reducer={name:'CompanyReducer', reducer:CompanyReducer};
    action = CompanyActions;
    constructor(
        private async:CompanyAsync
    ) {
        super();
        this.middlewares.push(
            this.async.accessCompany
        );
    }

    initialize():void {}
}