import { Meteor } from 'meteor/meteor';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';

import { Injectable } from '@angular/core';
import * as log from 'loglevel';

import { ICompanyActionPayload, ICompanyState} from "./company-redux.types";
import { CompanyActions } from "./company-redux.actions";

import {IPayloadAction} from "../../../common-app/src/ui/redux/action.interface";
import {Company} from "../../../hrm-app-api/api/models/company.model";

@Injectable()
export class CompanyAsync {
    accessCompany = (state: ICompanyState) => next => (action: IPayloadAction) => {
        let payload: ICompanyActionPayload = action.payload;
        switch (action.type) {
            case CompanyActions.ACCESS:
                Meteor.call('checkCompany', payload.nameCompany, (error:any, result:Company)=> {
                    if (error) {
                        log.error(error);
                        CompanyActions.error(CompanyActions.ACCESS_FAIL,error);
                    } else {
                        CompanyActions.accessCompanySuccess(result);
                    }
                });
                break;
            case CompanyActions.NEW_NAME:
                Meteor.call('checkCompanyExistByString', payload.nameCompany, (error:any, result:boolean)=> {
                    if (error) {
                        log.error('Check Company existed returned error');
                        log.error(error);
                        CompanyActions.error(CompanyActions.NEW_NAME_FAIL, error);
                    } else {
                        if(result){
                            CompanyActions.newNameSuccess(payload.nameCompany);
                        }
                    }
                });
                break;
            case CompanyActions.REGISTER:
                Meteor.call('registerCompany', payload.nameCompany, (error: any, result:Company) => {
                   if(error) {
                       log.error('Register Company returned error');
                       log.error(error);
                       CompanyActions.error(CompanyActions.REGISTER_FAIL, error);
                   } else {
                       if(result){
                           CompanyActions.registerCompanySuccess(result);
                       }
                   }
                });
            case CompanyActions.EDIT:
                Meteor.call('editCompany', payload.company, (error: any, result:Company) => {
                   if(error) {
                       log.error('Register Company returned error');
                       log.error(error);
                       CompanyActions.error(CompanyActions.EDIT_FAIL, error);
                   } else {
                       if(result){
                           CompanyActions.editCompanySuccess(result);
                       }
                   }
                });
        }
        return next(action);
    };
}
