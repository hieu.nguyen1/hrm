
import { Component, NgZone } from '@angular/core';
import { select } from 'ng2-redux';
import * as log from 'loglevel';


import {ILoginState} from "../../common-app/src/ui/redux/login/login.types";
import {LoginActions} from "../../common-app/src/ui/redux/login/login-actions.class";
import {Credentials} from "../../common-app/src/ui/services/credentials";
import {ICompanyState} from "../redux/company/company-redux.types";
import {Router} from "@angular/router";
import {CompanyActions} from "../redux/company/company-redux.actions";


@Component({
    templateUrl: 'login.user.template.html',
    selector: 'login-user'
})
export class RegisterCompanyComponent {
    @select() CompanyReducer;
    @select() loginReducer;
    nameCompany:string;
    state:ILoginState;
    credentials:Credentials;
    active:boolean = true;
    constructor(private ngZone:NgZone, private router: Router) {}
    ngOnInit() {
        this.CompanyReducer.subscribe((state:ICompanyState) => {
            if(state.nameCompany){
                this.nameCompany = state.nameCompany;
            } else {
                this.callToWelcomePage();
            }
        });
        this.loginReducer.subscribe( (state:ILoginState)=>{
            this.state = state;
        });
        this.credentials = Credentials.getLastCredentials();
    }
    ngAfterContentInit() {
        this.reset();
    }

    private reset() {
        this.active = false;  // Forces reset as per https://angular.io/docs/ts/latest/guide/forms.html
                              // This is a temporary workaround while we await a proper form reset feature.
        setTimeout(()=> this.active=true, 0);
    }

    register() {
        LoginActions.registerOwnerCompany(this.credentials, this.nameCompany);
        CompanyActions.registerCompany(this.nameCompany);
    }

    private callToWelcomePage(){
        this.ngZone.run(()=> {
            this.router.navigate(['/']);
        });
    }
    changeNameCompany(){
        this.callToWelcomePage();
    }

} 