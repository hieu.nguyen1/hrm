/**
 * Copyright Ken Ono, Fabrica Technolology 2016
 * Source code license under Creative Commons - Attribution-NonCommercial 2.0 Canada (CC BY-NC 2.0 CA)
 */

import { Component, NgZone, OnInit } from '@angular/core';
import { select } from 'ng2-redux';

import {LOGIN_INITIAL_STATE} from "../../common-app/src/ui/redux/login/login-reducer";
import {ILoginState} from "../../common-app/src/ui/redux/login/login.types";
import {ICompanyState} from "../redux/company/company-redux.types";
import {LOGIN_COMPANY_INITIAL_STATE} from "../redux/company/company-redux.reducer";

@Component({
    selector: 'top-frame-header',
    templateUrl: './top-frame-header.template.html'
})
export class TopFrameHeader implements OnInit {
    @select() loginReducer;
    @select() CompanyReducer;
    displayName:string;
    public nameCompany:string;

    constructor(private ngZone:NgZone) {}

    ngOnInit() {
        this.CompanyReducer.subscribe( (state:ICompanyState)=>{
            state = state || LOGIN_COMPANY_INITIAL_STATE;
            this.ngZone.run( ()=>{
                if(state.company)
                    this.nameCompany = state.company.name;
            } );
        } );
        this.loginReducer.subscribe( (state:ILoginState)=>{
            state = state || LOGIN_INITIAL_STATE;
            this.ngZone.run( ()=>{
                if(state.displayName)
                    this.displayName = state.displayName;
            } );
        } );
    }
}