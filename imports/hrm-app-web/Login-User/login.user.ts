
import { Component, NgZone } from '@angular/core';
import { select } from 'ng2-redux';
import * as log from 'loglevel';


import {ILoginState} from "../../common-app/src/ui/redux/login/login.types";
import {LoginActions} from "../../common-app/src/ui/redux/login/login-actions.class";
import {Credentials} from "../../common-app/src/ui/services/credentials";
import {ICompanyState} from "../redux/company/company-redux.types";
import {Company} from "../../hrm-app-api/api/models/company.model";
import {Router} from "@angular/router";


@Component({
  templateUrl: 'login.user.template.html',
  selector: 'login-user'
})
export class LoginUserComponent {
  @select() CompanyReducer;
  @select() loginReducer;

  company:Company;
  state:ILoginState;
  credentials:Credentials;
  active:boolean = true;
  constructor(private ngZone:NgZone, private router: Router) {}
  ngOnInit() {
    this.CompanyReducer.subscribe((state:ICompanyState) => {
      if(state.company){
        this.company = state.company;
      } else {
        this.ngZone.run(()=> {
          this.router.navigate(['/']);
        });
      }
    });
    this.loginReducer.subscribe( (state:ILoginState)=>{
      this.state = state;
    });
    this.credentials = Credentials.getLastCredentials();
  }
  ngAfterContentInit() {
    this.reset();
  }
  
  private reset() {
    this.active = false;  // Forces reset as per https://angular.io/docs/ts/latest/guide/forms.html
                          // This is a temporary workaround while we await a proper form reset feature.
    setTimeout(()=> this.active=true, 0);
  }

  login() {
    LoginActions.login(this.credentials);
  }

} 