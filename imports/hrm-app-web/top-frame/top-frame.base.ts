/**
 * Copyright Ken Ono, Fabrica Technolology 2016
 * Source code license under Creative Commons - Attribution-NonCommercial 2.0 Canada (CC BY-NC 2.0 CA)
 */

import {CompanyReduxModule} from "../redux";
import {IPayloadAction} from "../../common-app/src/ui/redux/action.interface";
import {LoginActions} from "../../common-app/src/ui/redux/login/login-actions.class";
import {CompanyActions} from "../redux/company/company-redux.actions";
import {ICompanyActionPayload} from "../redux/company/company-redux.types";
import {Company} from "../../hrm-app-api/api/models/company.model";

export abstract class TopFrame {

  addMiddlware(companyReduxModule:CompanyReduxModule) {
    // Middleware put here so it can have access to 'this.'.  This is a temporary work around until navigation with redux is done
    const navigatorMiddleware = store => next => (action:IPayloadAction) => {
      switch (action.type)  {
        case LoginActions.LOGGED_IN:
          if (!action.payload.autoLogin)
            //todo: need change to dashboad
            this.navigateToEditCompany();
          break;
        case LoginActions.LOGGED_OUT:
          this.navigateToWelcome();
          break;
        case CompanyActions.ACCESS_SUCCESS:
          this.navigateToLoginUser();
          break;
        case CompanyActions.NEW_NAME_SUCCESS: {
          this.navigateToRegisterCompany();
          break;
        }
        case CompanyActions.REGISTER_SUCCESS: {
          //
        }
        // case CompanyActions.VIEW_GAME_SUCCESS: {
        //   let forRealCardsPayload: IForRealCardsActionPayload = action.payload;
        //   this.navigateToGameTable(forRealCardsPayload.gameId);
        //   break;
        // }
      }
      return next(action);
    };


    companyReduxModule.middlewares.push(navigatorMiddleware);

  }
  abstract navigateToLoginUser():void;
  abstract navigateToEditCompany():void;
  // abstract navigateToGameTable(gameId:string):void;
  // abstract navigateToGamePlayer(gameId:string):void;
  abstract navigateToWelcome():void;
  abstract navigateToRegisterCompany():void;
}